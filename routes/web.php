<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','FrontController@index')->name('home');

Route::get('posts/{name?}/{post_type}','FrontController@showPostType')->name('posts');

Route::get('post/{post_type}/{post}', 'FrontController@show')->name('post');

Route::get('contact','FrontController@contact')->name('front_contact');

Route::post('search','FrontController@search');

//Route::get('posts/{type}/{name?}','FrontController@showPostType')->name('type');

Route::post('email','FrontController@sendEmail');

Auth::routes();

Route::middleware(['auth', ])->group(function () {
    Route::resource('admin/post','PostController');
    Route::post('delete','PostController@multipleDeletion')->name('multipleDeletion');
    Route::post('delete/definitive','PostController@definitiveDeletion')->name('definitiveDeletion');
    Route::get('trash','PostController@trash')->name('trash');
    Route::get('restore/{id}','PostController@restore')->name('restore');
});










//Route::get('/admin', 'PostController@index')->name('home');
