# SITE PROPOSANT DES STAGES ET FORMATIONS

Le site dispose d'une partie back permettant d'ajouter des formations ou des les éditer.

## La base de données

Voir le document DIAGRAMME_BDD.pdf

## Les fonctionnalités:

Partie front:

* Affichage des deux dernières formations ajoutées sur la page d'accueil
* Possibilité d'afficher individuellement une formation
* Un menu permettant d'accéder à la liste des formations ou des stages
* Une barre de recherche par titre ou description
* Une page contact

Partie back

* La liste de toutes les formations publiées ou non publiées
* Création d'une nouvelle formation
* Modification d'une formation
* Une corbeille avec possibilité de supprimer définitivement les éléments ou des les restaurer
* Barre de recherche dans tous les éléments y compris ceux non publiés