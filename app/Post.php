<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    
    // protected $fillable = [
    //     'title',
    //     'description',
    //     'category_id', 
    //     'post_type',
    //     'status',
    //     'price',
    //     'max_students_nb',
    //     'init_date',
    //     'end_date',
    // ];
    protected $guarded = [
        'img_title'
    ];
    protected $dates = ['deleted_at'];

    public function setCategoryIdAttribute($value){
        if($value == 0){
            $this->attributes['category_id'] = null;
        }else{
            $this->attributes['category_id'] = $value;
        }
    }
    
    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function picture(){
        return $this->hasOne(Picture::class);
    }

    public function scopeOfType($query,$type){
        return $query->where('post_type',$type);
    }

    public function scopePublished($query){
        return $query->where('status','published');
    }

    public function scopeSearch($query,string $word,$ability){
        $query = $query->where('title','like',"%$word%")
                       ->orwhere('description','like',"%$word%");
        if(!$ability)
            return $query->published();             
        return $query;
    }

}
