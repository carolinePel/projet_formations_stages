<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Post;
use Storage;
use App\Category;
use Cache;

class PostController extends Controller
{
    private $paginate = 5;

    /**
     * Display the two last added resources.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request){

        $posts = Post::orderBy('created_at','desc')->with('picture')->paginate($this->paginate);

        return view('back.post.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create(){
        
        $categories = Category::pluck('name','id');

        return view('back.post.create',['categories'=>$categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PostRequest|Request $request
     * @return \Illuminate\Http\Response
     */

    public function store(PostRequest $request){      
        
        $imgData = $request->only('picture');
        $post = Post::create($request->all());
        $this->uploadPicture($post, $request);
        //clear all cache
        Cache::flush();
        return redirect()->route('post.index')->with('message',[
            'type'   => 'success', 
            'content'=> "Ajout effectué!"
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Post $post
     * @return \Illuminate\Http\Response
     */

    public function edit(Post $post){
        
        $categories =  Category::pluck('name','id')->all();

        return view('back.post.edit',compact('post','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PostRequest|Request $request
     * @param Post $post
     * @return \Illuminate\Http\Response
     */

    public function update(PostRequest $request, Post $post){
        
        //$post = Post::find($id);
        $post->update($request->all());
        $this->uploadPicture($post,$request);
        Cache::flush();

        return redirect()->route('post.index')->with('message',[
            'type'    => 'success', 
            'content' => "Modification effectuée!"]);
    }

    /**
     * method to update or delete picture
     * @param Post $post
     * @param Request $request
     */

    private function uploadPicture(Post $post, Request $request):void{
        
        if(!empty($request->file('picture'))){
            $link = $request->file('picture')->store('./');

            if(!is_null($post->picture)){
                Storage::disk('local')->delete($post->picture->link);
                $post->picture()->delete();
            }
            $post->picture()->create([
                'link'=>$link,
                'title'=>$request->img_title??$request->title
            ]);

            return;
        }
    }

    /**
     * Show the trashed items.
     *
     * @return \Illuminate\Http\Response
     */

    public function trash(){

        $trashedPosts = Post::onlyTrashed()->paginate($this->paginate);

        return view('back.post.trash',compact('trashedPosts'));
    }

     /**
     * Remove the specified resources from storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */

    public function multipleDeletion(Request $request){

        $deleteCollect = collect($request->delete);
        $deleteCollect->each(function ($postId, $key) {
            $post = Post::find($postId);
            $post->delete();
        });
        Cache::flush();

        return redirect()->route('post.index')->with('message',[
            'type'    => 'success',
            'content' => 'Suppression effective!'
        ]);
    }

    public function definitiveDeletion(Request $request){
        
        $deleteCollect = collect($request->delete);
        $deleteCollect->each(function ($postId, $key) {
            $post = Post::onlyTrashed()->find($postId);
            $post->forceDelete();
        });
        Cache::flush();

        return redirect()->route('trash')->with('message',[
            'type'    => 'success',
            'content' => 'Suppression effective!'
        ]);
    }

    /**
     * Restore the specified resource from trash.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */

    public function restore(int $id){
        $post=Post::onlyTrashed()
            ->where('id', $id)
            ->restore();

        return redirect()->route('post.index')->with('message',[
            'type'    => 'success',
            'content' => "L'élément a bien été retiré de la corbeille!"
        ]);
    }


}
