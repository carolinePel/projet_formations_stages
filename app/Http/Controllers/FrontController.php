<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Post;
use App\User;
use Cache;
use Mail;

class FrontController extends Controller
{

    private $paginate = 5;

    
    /**
     * Display the two last added resources.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        //mettre en cache la pagination
        $prefix = request()->page?? '1';
        $key = 'post' . $prefix;
        $posts = Cache::remember($key, 60*24, function(){
            
            return Post::orderBy('init_date','desc')
                        ->with('picture')
                        ->limit(2)
                        ->published()
                        ->get();
        });

        // $posts = Post::orderBy('init_date','desc')
        //                 ->with('picture')
        //                 ->limit(2)
        //                 ->published()
        //                 ->get();

        return view('front.index',compact('posts'));
    }

    /**
     * Send an email
     * 
     * @return \Illuminate\Http\Response
     * @param Request $request
     */

    public function sendEmail(Request $request){

        $request->validate([
            'emailAdress' => 'email|required',
            'email'       => 'string|required'
        ]);
        
        Mail::send('emails.send', [
                'mail'        => $request->emailAdress,
                'mailContent' => $request->email
            ], function ($m)  {
            $m->from('hello@app.com', 'Your Application');           
            $m->to('tony@tony.fr', 'Tony')->subject('Your Reminder!');
        });

        return back()->with('message',[
            'type' => 'success',
            'content' => 'Le mail a bien été envoyé!'
        ]);
    }

    /**
     * Display posts by their type.
     *
     * @param string $post_type
     * @return \Illuminate\Http\Response
     */

    public function showPostType(string $post_type){

        $postsTypeList = Post::OfType($post_type)->published()->get();

        return view('front.postsList',compact('postsTypeList'));
    }

    /**
     * Display the two last added resources.
     *
     * @param string $type
     * @param Post $post
     * @return \Illuminate\Http\Response
     */

    public function show(string $type, Post $post){

        return view('front.show',compact('post'));
    }

    /**
     * Display contact page.
     *
     * @return \Illuminate\Http\Response
     */

    public function contact(){

        return view('front.contact');
    }

    /**
     * Look for a word in the resources
     * 
     * @return \Illuminate\Http\Response
     * @param Request $request
     */

    public function search(Request $request){

        $request->validate([
            'word' => 'string|required',
        ]);
        //dump($request->user());
        $ability = ($request->user()) ? true : false ;

        $posts = Post::search($request->word, $ability)->paginate($this->paginate);
    
        return view('front.searchResults',compact('posts'));
    }
}
