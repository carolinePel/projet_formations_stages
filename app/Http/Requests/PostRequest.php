<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'          => 'string|required',
            'description'    => 'string',
            'post_type'      => 'in:formation,stage',
            'init_date'      => 'date',
            'end_date'       => 'date|after:init_date',
            'category_id'    => 'integer|digits_between:0,4',
            'max_student_nb' => 'integer',
            'price'          => 'numeric|between:0,9999.99',
            'status'         => 'in:published,unpublished',
            'picture'        => 'mimes:jpeg,jpg|max:10000',
            'img_title'      => 'nullable|string'
        ];
    }
}
