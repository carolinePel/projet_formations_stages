<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title', 'Titre par défaut ...')</title>
    <link rel="stylesheet" href="{{url('https://use.fontawesome.com/releases/v5.2.0/css/all.css')}}" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row mt-4">
                    <div class="col-md-8">
                        <h1>Formations / Stages</h1>
                    </div>
                    <div class="float-md-right col">
                        @include('partials.authentication')
                    </div>
                </div>
                <div class="row mb-4">
                    <div class="col-md-5">
                        @include('partials.menu')
                    </div>
                    <div class="col-md-7">
                        @include('partials.search')
                    </div>
                </div>
                <div class="row">
                     <div class="col-12">
                        @yield('content')
                    </div>
                </div>
            <div>
        <div>
    </div>
    @section('scripts')
    <script src="{{asset('js/app.js')}}"></script>
    @show
</body>
</html>