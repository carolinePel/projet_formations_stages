@extends('layouts.master')

@section('title')
Ajouter un livre
@endsection
@section('content')
<form action="{{action('PostController@store')}}" method="post" enctype="multipart/form-data"/>
    {{ csrf_field() }}
<div class='row'>
        <div class='col'>
            <h2>Nouvelle formation / stage</h2>
        </div>
        <div class='col'>
            <div class="form-group float-right">
                <button type="submit" class="btn btn-primary">Ajouter</button>
            </div>
        </div>
    </div>

    <div class="form">
        <div class="form-group">
            <label for="type">Ajouter:</label>
            <select name="post_type" id='type' class="form-control">
                <option value="formation">une formation</option>
                <option value="stage">un stage</option>
            </select>
            @if($errors->has('post_type')) 
                <p class="error text-danger">
                    {{$errors->first('post_type')}}
                </p>
            @endif
        </div>

        <div class="form-group">
            <label for="title">Intitulé de la formation / du stage</label>
            <input  type="text" 
                    name="title" 
                    value="{{old('title')}}" 
                    class="form-control" 
                    id="title"
                    placeholder="titre">
            @if($errors->has('title')) 
                <p class="error text-danger">
                    {{$errors->first('title')}}
                </p>
            @endif
        </div>
    </div>

    <div class="form-group">
        <label for="description">Description :</label>
        <textarea   id="description" 
                    type="text" 
                    name="description" 
                    class="form-control" 
                    value="">{{old('description')}}
        </textarea>
        @if($errors->has('description')) 
            <p class="error text-danger">
                {{$errors->first('description')}}
            </p> 
        @endif
    </div>

<div class="form-row">
    <div class="col col-md-6">
        <label for="initDate">Date de début:</label>
        <input class="form-control" type="date" name="init_date" id="initDate">
        @if($errors->has('init_date')) 
            <p class="error text-danger">
                {{$errors->first('init_date')}}
            </p> 
        @endif
    </div>
    <div class="col col-md-6">
        <label for="endDate">Date de fin:</label>
        <input class="form-control" type="date" name="end_date" id="endDate">
        @if($errors->has('end_date')) 
            <p class="error text-danger">
                {{$errors->first('end_date')}}
            </p> 
        @endif
    </div>
</div>

<div class="form-row mt-3">
    <div class=" form-group col col-md-6">
        <label for="price">Prix (Euros):</label>
        <input class="form-control" 
               type="number" 
               step="0.01" 
               name="price" 
               id="price" 
               value="{{old('price')}}">
        @if($errors->has('price')) 
            <p class="error text-danger">
                {{$errors->first('price')}}
            </p> 
        @endif
    </div>

    <div class="form-group col col-md-6">
        <label for="maxNb">Nombre maximal d'élèves:</label>
        <input class="form-control" 
               type="number" 
               name="max_students_nb" 
               id="maxNb" 
               value="{{old('max_students_nb')}}">
        @if($errors->has('max_students_nb')) 
            <p class="error text-danger">
                {{$errors->first('max_students_nb')}}
            </p> 
        @endif
    </div>
</div>

<div class="form-row">
    <div class="form-group col col-md-6">
        <label for="category">Catégorie:</label>
        <select name="category_id" id='category' class="form-control">
            <option value="0" {{ is_null(old('category_id'))? 'selected' : '' }} >No category</option>
            @forelse($categories as $id=>$category)
                @if(old('category_id') == $id)
                        <option value="{{$id}}" selected>{{$category}}</option>
                    @else
                        <option value="{{$id}}">{{$category}}</option>
                    @endif
                    @empty
                    <option>No Category available</option>
            @endforelse
        </select>
        @if($errors->has('category_id')) 
            <p class="error text-danger">
                {{$errors->first('category_id')}}
            </p> 
        @endif
    </div>  
</div>      

    <div class="form-check">
        <input  id="publish" 
                type="radio" 
                @if(old('status')=='published') 
                    checked 
                @endif 
                name="status" 
                value="published" 
                checked>
        <label  for="publish" class="form-check-label">publier</label>
    </div>
    <div class="form-check">
        <input  id="unpublish" 
                type="radio" 
                @if(old('status')=='unpublished') 
                    checked 
                @endif 
                name="status" 
                value="unpublished" >
        <label  for="unpublish" class="form-check-label">dépublier</label>
    </div>
    @if($errors->has('status'))
        <p class="error text-danger">
            {{$errors->first('status')}}
        </p>
    @endif
    
    <div class="form-group mt-3">
        <h2 class="mb-3">Fichier :</h2>
        <label for="genre">Titre de l'image :</label>
        <div class="form-row">
            <div class="form-group col col-md-6 mt-3">
                <input class="form-control" 
                       type="text" 
                       name="img_title" 
                       value="{{old('img_title')}}">
                <input class="form-control-file mt-3" type="file" name="picture" >
                @if($errors->has('picture')) 
                    <p class="error text-danger">
                        {{$errors->first('picture')}}
                    </p> 
                @endif
            </div>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Ajouter</button>
    </div>


</form>
@endsection