@extends('layouts.master')

@section('title')
    Page d'accueil
@endsection

@section('content')
    @include('back.partials.flash')
    <div class="row ">
        <div class="col md-8">
            <h2>Corbeille</h2>
        </div>
        <div class="col float-right">
            <a class="float-right" href="{{route('post.index')}}">Retour vers la liste des formations</a>
        </div>
    </div>
    <p>{{$trashedPosts->links()}}</p>
    <form class="delete delete-mult" 
        method="POST" 
        action="{{route('definitiveDeletion')}}">
        {{ csrf_field() }}  
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Titre</th>
                    <th>Type</th>
                    <th>Début</th>
                    <th>Fin</th>
                    <th>Max étudiants</th>
                    <th>Categorie</th>
                    <th>prix</th>
                    <th>statut</th>
                    <th>supprimer</th>
                    <th>restaurer</th>
                </tr>
            </thead>

            <tbody>
                @forelse($trashedPosts as $key=>$post)
                <tr>
                    <td>{{$post->title}}</td>
                    <td>{{$post->post_type}}</td>
                    <td>{{$post->init_date}}</td>
                    <td>{{$post->end_date}}</td>
                    <td>{{$post->max_students_nb}}</td>
                    <td>{{$post->category->name}}</td>
                    <td>{{$post->price}}</td>
                    <td>{{$post->status}}</td>  
                    <td>
                        <input type="checkbox" name="delete[]" value="{{$post->id}}"/>{{$post->id}}
                    </td>         
                    <td>
                    <a href="{{action('PostController@restore',['id' => $post->id])}}">Restaurer</a>
                    </td>  
                </tr>
                @empty
                <li>Pas d'élément</li>
                @endforelse
            <tbody>
            
        </table>
        <div class='row'>
                    <div class="col col-md-8"> 
                        <p>{{$trashedPosts->links()}}</p>
                    </div>
                    <div class="col mt-3 float-right">
                        <button class="delete btn btn-lg text-white bg-danger float-right " type="submit">
                            supprimer définitivement
                        </button>
                    </div>
                </div>
        </form>

@endsection

@section('scripts')
    @parent
    <script src="{{url('storage', ['js', 'confirm.js'])}}"></script>
@endsection