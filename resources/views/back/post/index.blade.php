@extends('layouts.master')

@section('title')
    Page d'accueil
@endsection

@section('content')
    <h2>Liste des formations</h2>
    <p>{{$posts->links()}}</p>
    @include('back.partials.flash')
    <div class="row">
        <div class="col">
            <p>
                <a href="{{route('post.create')}}">
                    <button type="button" class="btn btn-primary btn-lg">
                        Ajouter une formation
                    </button>
                </a>
            </p>
        </div>
        <div class="col col-md-2">
            <a class="float-right" href="{{route('trash')}}">
                <button class="btn btn-lg bg-primary float-right text-white">
                Voir les éléments supprimés
                </button>
            </a>
        </div>
    </div>
    <div class="row">
            <div class="col mb-2 float-right">
                <button class="btn btn-lg text-white bg-warning float-right " type="submit">
                    supprimer
                </button>
            </div>
    </div>
        
    <div class="row"> 
        <form class="delete-mult" 
            method="POST" 
            action="{{route('multipleDeletion')}}">
            {{ csrf_field() }}      

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Titre</th>
                        <th>Type</th>
                        <th>Début</th>
                        <th>Fin</th>
                        <th>Max étudiants</th>
                        <th>Categorie</th>
                        <th>prix</th>
                        <th>statut</th>
                        <th>voir</th>
                        <th>éditer</th>
                        <th>supprimer</th>
                    </tr>
                </thead>
                
                <tbody>
                    @forelse($posts as $key=>$post)
                    <tr>
                        <td>{{$post->title}}</td>
                        <td>{{$post->post_type}}</td>
                        <td>{{$post->init_date}}</td>
                        <td>{{$post->end_date}}</td>
                        <td>{{$post->max_students_nb}}</td>
                        <td>{{$post->category->name??'pas de categorie'}}</td>
                        <td>{{$post->price}}</td>
                        <td>{{$post->status}}</td>
                        <td><a href="{{route('post', ['post_type' => $post->post_type ,'id' => $post->id])}}">
                                <i class="fas fa-eye"></i>
                                <span class="sr-only">afficher<span>
                            </a>
                        </td>
                        <td><a href="{{route('post.edit',$post->id)}}">
                                <i class="fas fa-edit"></i>
                                <span class="sr-only">edit</span>
                            </a>
                        </td>         
                        <td>
                            <input type="checkbox" name="delete[]" value="{{$post->id}}"/>{{$post->id}}
                        </td>
                    </tr>
                    @empty
                    <li>Pas d'élément</li>
                    @endforelse
                </tbody>
            </table>

            <div class='row'>
                <div class="col col-md-8"> 
                    <p>{{$posts->links()}}</p>
                </div>
                <div class="col mt-3 float-right">
                    <button class="btn btn-lg text-white bg-warning float-right " type="submit">
                        supprimer
                    </button>
                </div>
            </div>
        </form>


@endsection
