@extends('layouts.master')

@section('title')
    @if($post->post_type == "formation")
        Modifier une formation
    @else
        Modifier un Stage
    @endif
@endsection

@section('content')
<form action="{{action('PostController@update', ['id' => $post->id])}}" method="post" enctype="multipart/form-data"/>
        {{ csrf_field() }}
        {{method_field('PUT')}}
    <div class='row'>
        <div class='col'>
            <h2>
                @yield('title')
            </h2>
        </div>
        <div class='col'>
            <div class="form-group float-right">
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </div>
    </div>


        <div class="form">
            <div class="form-group">
                <label for="type">Type:</label>
                <select name="post_type" id='type' class="form-control">
                    <option value="formation"{{($post->post_type == 'stage')?? 'selected'}}>une formation</option>
                    <option value="stage" {{($post->post_type == 'formation')?? 'selected'}}>un stage</option>
                </select>
                @if($errors->has('post_type')) 
                    <p class="error text-danger">
                        {{$errors->first('post_type')}}
                    </p>
                @endif
            </div>

            <div class="form-group">
                <label for="title">Intitulé de la formation / du stage</label>
                <input  type="text" 
                        name="title" 
                        value="{{$post->title}}" 
                        class="form-control" 
                        id="title"
                        placeholder="titre">
                @if($errors->has('title')) 
                    <p class="error text-danger">
                        {{$errors->first('title')}}
                    </p>
                @endif
            </div>
        

            <div class="form-group">
                <label for="description">Description :</label>
                <textarea   id="description" 
                            type="text" 
                            name="description" 
                            class="form-control" 
                            value="">
                            {{$post->description}}
                            </textarea>
                @if($errors->has('description')) 
                    <p class="error text-danger">
                        {{$errors->first('description')}}
                    </p> 
                @endif
            </div>

            <div class="form-row">
                <div class="col col-md-6">
                    <label for="initDate">Date de début:</label>
                    <input class="form-control" type="date" name="init_date" id="initDate" value="{{$post->init_date}}">
                    @if($errors->has('init_date')) 
                        <p class="error text-danger">{{$errors->first('init_date')}}</p> 
                    @endif
                </div>
                <div class="col col-md-6">
                    <label for="endDate">Date de fin:</label>
                    <input class="form-control" type="date" name="end_date" id="endDate" value="{{$post->end_date}}">
                    @if($errors->has('end_date')) 
                        <p class="error text-danger">{{$errors->first('end_date')}}</p> 
                    @endif
                </div>
            </div>


            <div class="form-row mt-3">
                <div class=" form-group col col-md-6">
                    <label for="price">Prix (Euros):</label>
                    <input class="form-control" type="number" name="price" id="price" value="{{$post->price}}" >
                    @if($errors->has('price')) 
                        <p class="error text-danger">{{$errors->first('price')}}</p> 
                    @endif
                </div>
                <div class="form-group col col-md-6">
                    <label for="maxNb">Nombre maximal d'élèves:</label>
                    <input class="form-control" type="number" name="max_students_nb" id="maxNb" value="{{$post->max_students_nb}}">
                    @if($errors->has('max_students_nb')) 
                        <p class="error text-danger">{{$errors->first('max_students_nb')}}</p> 
                    @endif
                </div>
            </div>

            <div class="form-row">
                <div class="form-group col col-md-6">
                    <label for="category">Catégorie:</label>
                    <select name="category_id" id='category' class="form-control">
                        <option value="0" {{is_null($post->category_id)? 'selected' : ''}}>No category</option>
                        @foreach($categories as $id => $name)
                            <option {{ ($post->category_id == $id)? 
                                            'selected' : 
                                            '' 
                                    }}  
                                    value="{{$id}}">
                                    {{$name}}
                            </option>
                        @endforeach
                    </select>
                    @if($errors->has('category_id')) 
                        <p class="error text-danger">{{$errors->first('category_id')}}</p> 
                    @endif
                </div>  
            </div>      

            <div class="form-check">
                <input  type="radio"
                        class="form-check-input" 
                        id='publish'
                        @if($post->status =='published') 
                            checked 
                        @endif 
                        name="status" 
                        value="published"
                        checked
                        >
                <label  for="publish" class="form-check-label">publier</label>
            </div>

            <div class="form-check">
                <input  type="radio" 
                        class="form-check-input" 
                        id='unpublish'
                        @if($post->status =='unpublished')
                            checked 
                        @endif 
                        name="status" 
                        value="unpublished" > 
                <label  for="unpublish" class="form-check-label">dépublier</label>
            </div>
            @if($errors->has('status'))
                <p class="error text-danger">
                    {{$errors->first('status')}}
                </p>
            @endif

            <div class="form-group row mt-3">
                {{--Si il existe déjà une image--}}
                @if($post->picture)
                    <div class="col mb-6">
                        <h2 class="mb-3">Modifier l'image :</h2>
                        <img width="271px" 
                            src="{{url('storage/images/'.$post->picture->link)}}" 
                            alt="{{$post->picture->title}}"/>
                    </div>
                    <div class="col">
                        <div class="form-row">
                            <div class="form-group col col-md-6 mt-3">
                                <label for="titre">Titre d'image :</label>
                                <input class="form-control" 
                                    id="titre" 
                                    type="text" 
                                    name="img_title" 
                                    value="{{$post->picture->title}}">
                                <input class="mt-3 form-control-file" type="file" name="picture" >
                            </div>
                        </div>
                    </div>

                {{--Si il n'y a pas d'image--}}
                @else
                    <div class="col mb-12">
                        <h2>Ajouter une image:</h2>
                        <label for="titre">Titre d'image :</label>
                        <div class="form-row">
                            <div class="form-group col col-md-6 mt-3">
                                <input class="form-control" 
                                    id="titre" 
                                    type="text" 
                                    name="img_title" 
                                    value="" 
                                    placeholder="titre de l'image"> 
                                <input class="mt-3 form-control-file" type="file" name="picture" >
                            </div>
                        </div> 
                    </div>   
                @endif


                        
                @if($errors->has('picture')) 
                    <p class="error text-danger">
                        {{$errors->first('picture')}}
                    </p> 
                @endif
                @if($errors->has('img_title'))
                    <p class="error text-danger">
                        {{$errors->first('img_title')}}
                    </p> 
                @endif
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-primary">Modifier</button>
            </div>
        </div>
    </form>

@endsection