<li class="list-group-item">
    <div class="row">
        @if(!is_null($post->picture))
        <div class="col-xs-6 col-md-3">
            <a href="#" class="thumbnail">
            <img width="171" 
                 src="{{url('storage', ['images', $post->picture->link])}}" 
                 alt="{{$post->picture->title}}">
            </a>
        </div>
        @endif
        <div class="col-xs-6 col-md-9">
            <h2>
                <a href="{{route('post', ['post_type' => $post->post_type ,'id' => $post->id])}}">
                    {{$post->title}}
                </a>
            </h2>
            {{$post->description}}
            <p>{{$post->post_type}}</p>
            <p>{{$post->id}}</p>
            <p>{{$post->status}}</p>
        </div>
    </div>
    <p>Date de début: {{$post->init_date}}</p>
</li>