@if (Auth::check())
    <a  href="{{route('post.index')}}">Tableau de bord</a> 
    <a  class="float-md-right"
        href="{{route('logout')}}"
        onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
        Se déconnecter
    </a> 
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
    </form>            
    
@else
    <a class="float-right" href="{{ route('login') }}">Se connecter</a>
@endif