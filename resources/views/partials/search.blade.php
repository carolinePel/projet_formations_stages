<form   method="POST" 
        action="{{action('FrontController@search')}}" 
        class="form-inline float-md-right my-2 my-lg-0">
    {{ csrf_field() }}
    @if($errors->has('word'))
        <p class="error my-auto text-danger mr-sm-2">
            {{$errors->first('word')}}
        </p> 
    @endif
    <input  name="word" 
            class="form-control mr-sm-2" 
            type="search" 
            placeholder="Rechercher" 
            aria-label="Rechercher"
    >
    <button class="btn btn-outline-success" type="submit">Rechercher</button>
</form>