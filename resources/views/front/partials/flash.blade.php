@if(Session::has('message'))
<div class="{{ Session::get('message')['type']}}">
    <p>{{Session::get('message')['content']}}</p>
</div>
@endif