@extends('layouts.master')

@section('title')
    Page d'accueil
@endsection

@section('content')

<ul class="list-group">
    @forelse($posts as $post)
        @include('partials.showOneListItem')
    @empty
        <li>Rien à afficher</li>
    @endforelse
</ul>
@endsection