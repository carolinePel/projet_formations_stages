@extends('layouts.master')

@section('title')
    Page de {{$post->post_type}}
@endsection

@section('content')
    <h2 class="mb-4 text-capitalize">{{$post->post_type}} de {{$post->title}}</h2>
    <div class="row mb-4">
        @if(!is_null($post->picture))
        <div class="col-md-4">
            <a href="#" class="thumbnail">
            <img width="271" src="{{url('storage', ['images', $post->picture->link])}}" alt="{{$post->picture->title}}">
            </a>
        </div>
        @endif
        <div class="col">
            description: {{$post->description}}
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 col-md-4">
            <p>Date de début: {{$post->init_date}}</p>
            <p>Date de fin: {{$post->end_date}}</p>
        </div>
        <div class="col">
            <p>Nombre d'élèves max par classe: {{$post->max_students_nb}}</p>
        </div>
    </div>
@endsection