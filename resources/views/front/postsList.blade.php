@extends('layouts.master')

@section('title')
    Liste des éléments
@endsection

@section('content')
    <ul class="list-group">
        @forelse($postsTypeList as $post)
            @include('partials.showOneListItem')
        @empty
            <li>Pas d'élément</li>
        @endforelse
    </ul>
@endsection