@extends('layouts.master')

@section('title')
Page de contact
@endsection

@section('content')

  @include('front.partials.flash')

  <h2>Contact</h2>
  <form action="{{action('FrontController@sendEmail')}}" method="post"/>
  {{csrf_field()}}
    <div class="form-group">
      <label for="formGroupExampleInput">Email</label>
      <input name="emailAdress" 
             value="" 
             type="email" 
             class="form-control" 
             id="formGroupExampleInput" 
             placeholder="email" 
             required>
    </div>
    <div class="form-group">
      <label for="exampleFormControlTextarea1">Description</label>
      <textarea name="email" 
                value="" 
                class="form-control" 
                id="exampleFormControlTextarea1" 
                rows="3" 
                placeholder="votre message" 
                required>
      </textarea>
    </div>
    <button type="submit" class="btn btn-primary mb-2">
      Envoyer
    </button>

  </form>

@endsection