<?php

use Illuminate\Database\Seeder;
//use Faker\Generator as Faker;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //private $faker;
    public function run()
    {
        //suppression des images avant de fabrication
        Storage::disk('local')->delete(Storage::allFiles());

        /**CREATION CATAGORIES */
        App\Category::insert([
            [ 'name' => 'astronaute' ],
            [ 'name' => 'cuisiniere' ],
            [ 'name' => 'psychiatre' ],
            [ 'name' => 'grutiere'],
        ]);

        /**CREATION POSTS */
        factory(App\Post::class, 10)->create()->each(function($post){
            
            /**ASSIGNATION CATEGORIES POUR LES POSTS */

            //on crée au hasard la catégorie
            $category = App\Category::find(rand(1,4));
            //on associe à chaque post une catégorie
            $post->category()->associate($category);
            $post->save();


            /**AJOUT IMAGES */
            //pour chaque post créé, on lui associe une image
            //créer un hash de lien pour le nom de l'image
            $link = str_random(12) . '.jpeg';
            $file = file_get_contents('http://via.placeholder.com/200/200/'.rand(1,9));
            
            //déplacer le flux récupéré
            Storage::disk('local')->put($link,$file);
            //enregistrement dans la table picture
            $post->picture()->create([
                'title'=> 'Default',
                'link' => $link
            ]);
        });
    }
}
