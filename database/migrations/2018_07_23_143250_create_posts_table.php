<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->enum('post_type',['formation','stage'])->default('formation');
            $table->string('title',100);
            $table->text('description');
            $table->unsignedDecimal('price', 6, 2)->nullable();
            $table->unsignedSmallInteger('max_students_nb')->nullable();
            $table->enum('status',['published','unpublished'])->default('unpublished');
            $table->date('init_date');
            $table->date('end_date');
            $table->unsignedInteger('category_id')->nullable();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('SET NULL'); // Contrainte référencé sur la table genres.id

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //$table->dropForeign('posts_categories_id_foreign');
        //$table->dropColumn('categories_id');
        Schema::dropIfExists('posts');
    }
}
