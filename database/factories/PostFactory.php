<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    //type de post
    $post_type = (rand(1,2) == 1)? 'formation' : 'stage';
    //date de début
    $init_date = $faker->dateTimeInInterval($startDate = 'now', $interval = '+ 2 years', $timezone = null)->format('Y-m-d');
    $duration  = sprintf('+ %d months',rand(1,12));
    //date de fin crée à partir de la date de début
    $end_date  = $faker->dateTimeInInterval($startDate = $init_date, $interval = $duration , $timezone = null)->format('Y-m-d') ;
    //pour le random:$faker->randomElement($array = array('formation','stage'));
    return [
        'title'          => $faker->sentence(),
        'description'    => $faker->paragraph(),
        'status'         => 'published',
        'post_type'      => $post_type,
        'price'          => rand(0,9999),
        'max_students_nb' => rand(5,40),
        'init_date'      => $init_date,
        'end_date'       => $end_date    
    ];
});
